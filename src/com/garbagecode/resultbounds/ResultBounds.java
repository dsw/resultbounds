package com.garbagecode.resultbounds;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

/**
 * 
 * 这个类干嘛用的，请参考 PaginationInterceptor.intercept 方法
 *
 */
public class ResultBounds extends RowBounds {
  private String order;          // 分页用的比如 "xx asc"、"xx asc, yy desc"
  private TotalCount totalCount; // 分页插件里会给对象 ResultBounds 的这个字段设置一个具体的 TotalCount 对象

  public ResultBounds() {

  }

  public ResultBounds(String order) {
    this.order = order;
  }

  public ResultBounds(RowBounds rowBounds) {
    super(rowBounds.getOffset(), rowBounds.getLimit());
  }

  public ResultBounds(int offset, int limit) {
    super(offset, limit);
  }

  public ResultBounds(RowBounds rowBounds, String order) {
    super(rowBounds.getOffset(), rowBounds.getLimit());
    this.order = order;
  }

  public ResultBounds(int offset, int limit, String order) {
    super(offset, limit);
    this.order = order;
  }

  public String getOrder() {
    return order;
  }

  public void setOrder(String order) {
    this.order = order;
  }

  /**
   * 返回符合查询条件的记录总数
   * @param sqlSession
   * @return
   */
  public long getTotal(SqlSession sqlSession) {
    if (totalCount == null) {
      throw new RuntimeException("totalCount' is null");
    }

    return totalCount.getTotal(sqlSession);
  }

  public void setTotalCount(TotalCount totalCount) {
    this.totalCount = totalCount;
  }

}
