package com.garbagecode.resultbounds.dialect;

public interface Dialect {

  /**
   * 返回分页的 SQL 语句
   * @param sql
   * @param offset
   * @param limit
   * @param order
   * @return
   */
  String getPaginationSql(String sql, int offset, int limit, String order);

  /**
   * 返回统计总数的 SQL 语句
   * @param sql
   * @return
   */
  String getTotalCountSql(String sql);

}
