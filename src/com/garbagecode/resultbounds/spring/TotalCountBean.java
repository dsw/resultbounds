package com.garbagecode.resultbounds.spring;

import org.apache.ibatis.session.SqlSession;

import com.garbagecode.resultbounds.TotalCount;

/**
 * 
 * 若把这个对象赋值给 ResultBounds 对象的 totalCount 字段，
 * 那么调用 ResultBounds 对象的 getTotal 方法时就只需要给个 null 参数就可以了
 * 比如这样:
 * ResultBounds resultBounds = new ResultBounds();
 * TotalCountBean totalCountBean = ...
 * resultBounds.setTotalCount(totalCountBean);
 * long total = resultBounds.getTotal(null);
 */
public class TotalCountBean extends TotalCount {
  private static SqlSession sqlSession;

  public TotalCountBean() {
    super(null, null);
  }
  
  public TotalCountBean(String statement, Object parameter) {
    super(statement, parameter);
  }
  
  public void setSqlSession(SqlSession sqlSession) {
    TotalCountBean.sqlSession = sqlSession;
  }

  @Override
  public long getTotal(SqlSession sqlSession) {
    return sqlSession == null 
        ? super.getTotal(TotalCountBean.sqlSession) 
        : super.getTotal(sqlSession);
  }

}
