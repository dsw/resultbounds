package com.garbagecode.resultbounds.spring;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import com.garbagecode.resultbounds.PaginationInterceptor;
import com.garbagecode.resultbounds.ResultBounds;

/**
 * 使用这个插件除了在 MyBatis 配置文件上配上这个插件外，还需要
 * 在 Spring 配置文件中加上 TotalCountBean，就像这样
 * <bean class="com.garbagecode.resultbounds.spring.TotalCountBean">
 *  <property name="sqlSession" ref="sqlSession" />
 * </bean>
 */
@Intercepts({ @Signature(type = Executor.class, 
method = "query", 
args = { MappedStatement.class, 
         Object.class,
         RowBounds.class, 
         ResultHandler.class }) })
public class TotalCountBeanInterceptor extends PaginationInterceptor {

  @Override
  protected void setTotal(ResultBounds resultBounds, MappedStatement mappedStatement, Object parameter, Invocation invocation) {
    resultBounds.setTotalCount(new TotalCountBean(mappedStatement.getId(), parameter));
  }
  
}
