resultbounds - MyBatis分页插件
==================================================

### 该插件的可取之处 ###

以简单的方式提供一个优雅的 MyBatis 物理分页功能，不需要修改代码就可以把默认的分页换成物理分页。目前该插件的实现只有几个类并且加了注释，对想了解 MyBatis 分页插件的以及想自己实现一个的，自认为还是挺值得参考的。 

### 该插件支持的 MyBatis 版本 ###
```
MyBatis 3.4: 支持  (在 MyBatis-3.4.4 版本下测试通过)
MyBatis 3.3: 支持  (在 MyBatis-3.3.1 版本下测试通过)
MyBatis 3.2: 支持  (在 MyBatis-3.2.8 版本下测试通过)
MyBatis 3.1: 支持  (在 MyBatis-3.1.1 版本下测试通过)
MyBatis 3.0: 不支持(在 MyBatis-3.0.6 版本下测试失败)
MyBatis 2.3: 不支持(在 MyBatis-2.3.5 版本下测试失败)
```

### 在 MyBatis 配置文件中加上这个分页插件 ###
```
<configuration>
  ...
  <plugins>
    <plugin interceptor="com.garbagecode.resultbounds.PaginationInterceptor">
      <!-- 可以把 value 属性值替换成自己实现的 Dialect -->
      <property name="dialect" value="com.garbagecode.resultbounds.dialect.MySqlDialect" />
    </plugin>
  </plugins>
  ...
</configuration>
  ```

### 在代码中使用这个插件进行分页的示例 ###
```
// 示例一
// 获取 SqlSession 对象
SqlSession session = ...
// 从第 4 条开始，取 5 条记录
List<Student> studentList = session.selectList("test.student.getStudentList", null, new RowBounds(3, 5));

// 示例二
// 获取 SqlSession 对象
SqlSession session = ...
// 从第 4 条开始，取 5 条记录
List<Student> studentList = session.getMapper(StudentMapper.class).getStudentList(new RowBounds(3, 5));
```   

### 在查询的同时统计记录总数 ###
```
// 示例一
// 获取 SqlSession 对象
SqlSession session = ...
// 设置分页参数从第 4 条开始，取 5 条记录
ResultBounds resultBounds = new ResultBounds(3, 5);
// 获取分页后的查询结果
List<Student> studentList = session.selectList("test.student.getStudentList", null, resultBounds);
// 获取记录总数
long total = resultBounds.getTotal(session);

// 示例二
// 获取 SqlSession 对象
SqlSession session = ...
// 设置分页参数从第 4 条开始，取 5 条记录
ResultBounds resultBounds = new ResultBounds(3, 5);
// 获取分页后的查询结果
List<Student> studentList = session.getMapper(StudentMapper.class).getStudentList(resultBounds);
// 获取记录总数
long total = resultBounds.getTotal(session);
```   

### 若使用了 mybatis-spring-xxxx.jar 包整合了 Spring ###
在 MyBatis 配置文件中加上该插件的 Spring 版本
```
<configuration>
  ...
  <plugins>
    <plugin interceptor="com.garbagecode.resultbounds.spring.TotalCountBeanInterceptor">
      <property name="dialect" value="com.garbagecode.resultbounds.dialect.MySqlDialect" />
    </plugin>
  </plugins>
  ...
</configuration>
```
在 Spring 配置文件中加上这个
```
...
<bean class="com.garbagecode.resultbounds.spring.TotalCountBean">
  <property name="sqlSession" ref="sqlSession" />
</bean>
...
```
使用该插件的 Spring 版本后
```
...
// 获取记录总数
// 调用 ResultBounds.getTotal 方法的时候不需要给 SqlSession 参数了
long total = resultBounds.getTotal(null);
...
```


### 说明该插件各个类是干嘛用的 ###
```
---- dialect
  |---- Dialect                     生成分页语句、统计记录总数语句的接口
  |---- MySqlDialect                Dialect 接口的 MySql 实现
---- spring                             
  |---- TotalCountBean
  |---- TotalCountBeanInterceptor   这插件的 spring 版本
---- MyBatisClassCreator            创建 MyBatis 中的类，在 PaginationInterceptor 中需要用它对参数动些手脚
---- PaginationInterceptor          分页的主逻辑在这个类里
---- ResultBounds                   RowBounds 的子类，加了一些实用的方法
---- TotalCount                     用来统计记录总数
```