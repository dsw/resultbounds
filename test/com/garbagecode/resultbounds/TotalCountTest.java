package com.garbagecode.resultbounds;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.garbagecode.resultbounds.testdependency.MyBatisTestCase;

public class TotalCountTest extends MyBatisTestCase {

  @Test
  public void testGetTotal() {
    SqlSession session = getSession();
    
    {
       TotalCount totalCount = new TotalCount("test.student.getStudentList", null);
       Long total = totalCount.getTotal(session);
       
       assertEquals(10L, (long) total);
    }
    
    // 动态 SQL 语句
    {
      Map<String, Object> paramMap = new HashMap<String, Object>();
      paramMap.put("minAge", 20);
      paramMap.put("maxAge", 23);
      
      TotalCount totalCount = new TotalCount("test.student.getStudentListBy", paramMap);
      Long total = totalCount.getTotal(session);
      
      assertEquals(7L, (long) total);
    }
    
    // 嵌套查询
    {
      TotalCount totalCount = new TotalCount("test.score.getScoreList", null);
      Long total = totalCount.getTotal(session);
      
      assertEquals(4L, (long) total);
    }
  }
  
}
