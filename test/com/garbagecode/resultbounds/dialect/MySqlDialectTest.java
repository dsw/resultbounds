package com.garbagecode.resultbounds.dialect;

import static org.junit.Assert.*;

import org.apache.ibatis.session.RowBounds;
import org.junit.Test;

public class MySqlDialectTest {

  @Test
  public void testGetPaginationSql() {
    MySqlDialect dialect = new MySqlDialect();
    
    assertEquals("SELECT * FROM user", 
        dialect.getPaginationSql("SELECT * FROM user", RowBounds.NO_ROW_OFFSET, RowBounds.NO_ROW_LIMIT, null));
    
    assertEquals("SELECT * FROM user WHERE age > 20 AND age < 30 ORDER BY age DESC LIMIT 20,10", 
        dialect.getPaginationSql("SELECT * FROM user WHERE age > 20 AND age < 30", 20, 10, "age.DESC"));
  }
  
  @Test
  public void testGetTotalCountSql() {
    MySqlDialect dialect = new MySqlDialect();
    
    assertEquals("SELECT COUNT(*) FROM user", 
        dialect.getTotalCountSql("SELECT * FROM user"));
  }
  
}
