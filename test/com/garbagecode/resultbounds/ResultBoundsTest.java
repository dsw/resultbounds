package com.garbagecode.resultbounds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.garbagecode.resultbounds.testdependency.MyBatisTestCase;
import com.garbagecode.resultbounds.testdependency.Score;
import com.garbagecode.resultbounds.testdependency.Student;
import com.garbagecode.resultbounds.testdependency.mapper.StudentMapper;

public class ResultBoundsTest extends MyBatisTestCase {

  @Test
  public void testGetTotal() {
    SqlSession session = getSession();
    
    // 分页
    {
      ResultBounds resultBounds = new ResultBounds(3, 5);
      resultBounds.setOrder("age.desc");
      
      List<Student> studentList = session.selectList("test.student.getStudentList", null, resultBounds);
      
      assertEquals(10, resultBounds.getTotal(session));
      assertEquals(5, studentList.size());
      assertEquals("刘一", studentList.get(0).getName());
      assertEquals("张三", studentList.get(1).getName());
      assertEquals("王五", studentList.get(2).getName());
      assertEquals("孙七", studentList.get(3).getName());
      assertEquals("赵六", studentList.get(4).getName());
    }
    
    // 对动态 SQL 语句分页
    {
      ResultBounds resultBounds = new ResultBounds(3, 5);
      resultBounds.setOrder("age.desc");
      Map<String, Object> paramMap = new HashMap<String, Object>();
      paramMap.put("minAge", 21);
      paramMap.put("maxAge", 25);
      List<Student> studentList = session.selectList("test.student.getStudentListBy", paramMap, resultBounds);
      
      assertEquals(8, resultBounds.getTotal(session));
      assertEquals(5, studentList.size());
      assertEquals("张三", studentList.get(0).getName());
      assertEquals("王五", studentList.get(1).getName());
      assertEquals("孙七", studentList.get(2).getName());
      assertEquals("陈二", studentList.get(3).getName());
      assertEquals("赵六", studentList.get(4).getName());
    }
    
    // 嵌套查询
    {
      ResultBounds resultBounds = new ResultBounds(1, 2);
      List<Score> scoreList = session.selectList("test.score.getScoreList", null, resultBounds);
      
      assertEquals(4, resultBounds.getTotal(session));
      assertEquals(2, scoreList.size());
      
      Score score1 = scoreList.get(0);
//      assertEquals("数学", score1.getSubject());
      assertEquals(89F, (float) score1.getScore(), 0.01F);
      assertNotNull(score1.getStudent());
      assertEquals(3, (int) score1.getStudent().getId());
      assertEquals("张三", score1.getStudent().getName());
      assertEquals(22, (int) score1.getStudent().getAge());
      
      Score score2 = scoreList.get(1);
//      assertEquals("语文", score2.getSubject());
      assertEquals(92F, (float) score2.getScore(), 0.01F);
      assertNotNull(score2.getStudent());
      assertEquals(4, (int) score2.getStudent().getId());
      assertEquals("李四", score2.getStudent().getName());
      assertEquals(25, (int) score2.getStudent().getAge());
    }
    
    // 对 Mapper 接口查询分页
    {
      ResultBounds resultBounds = new ResultBounds(3, 5);
      resultBounds.setOrder("age.desc");
      
      List<Student> studentList = session.getMapper(StudentMapper.class).getStudentList(resultBounds);
      
      assertEquals(10, resultBounds.getTotal(session));
      assertEquals(5, studentList.size());
      assertEquals("刘一", studentList.get(0).getName());
      assertEquals("张三", studentList.get(1).getName());
      assertEquals("王五", studentList.get(2).getName());
      assertEquals("孙七", studentList.get(3).getName());
      assertEquals("赵六", studentList.get(4).getName());
    }
  }
  
}
