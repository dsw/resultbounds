package com.garbagecode.resultbounds;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.garbagecode.resultbounds.dialect.MySqlDialectTest;

@RunWith(Suite.class)
@SuiteClasses({ MySqlDialectTest.class, TotalCountTest.class, ResultBoundsTest.class, PaginationInterceptorTest.class })
public class AllTests {

}
