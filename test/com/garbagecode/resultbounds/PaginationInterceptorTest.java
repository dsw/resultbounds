package com.garbagecode.resultbounds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import com.garbagecode.resultbounds.dialect.MySqlDialect;
import com.garbagecode.resultbounds.testdependency.MyBatisTestCase;
import com.garbagecode.resultbounds.testdependency.Score;
import com.garbagecode.resultbounds.testdependency.Student;
import com.garbagecode.resultbounds.testdependency.mapper.StudentMapper;

public class PaginationInterceptorTest extends MyBatisTestCase {
  
  @Test
  public void testSetProperties() {
    Properties properties = new Properties();
    properties.setProperty(PaginationInterceptor.PROPERTY_DIALECT_NAME, "com.garbagecode.resultbounds.dialect.MySqlDialect");
  
    PaginationInterceptor interceptor = new PaginationInterceptor();
    interceptor.setProperties(properties);
    
    assertTrue(interceptor.getDialect() instanceof MySqlDialect);
  }
  
  @Test
  public void test() {
    SqlSession session = getSession();
    
    // 不分页
    {
      RowBounds rowBounds = new RowBounds();
      List<Student> studentList = session.selectList("test.student.getStudentList", null, rowBounds);
      
      assertEquals(10, studentList.size());
    }
    
    // 分页
    {
      RowBounds rowBounds = new RowBounds(3, 5);
      List<Student> studentList = session.selectList("test.student.getStudentList", null, rowBounds);
      
      assertEquals(5, studentList.size());
      assertEquals("李四", studentList.get(0).getName());
      assertEquals("王五", studentList.get(1).getName());
      assertEquals("赵六", studentList.get(2).getName());
      assertEquals("孙七", studentList.get(3).getName());
      assertEquals("周八", studentList.get(4).getName());
    }
    
    // 对动态 SQL 语句分页
    {
      RowBounds rowBounds = new RowBounds(3, 5);
      Map<String, Object> paramMap = new HashMap<String, Object>();
      paramMap.put("minAge", 21);
      paramMap.put("maxAge", 25);
      List<Student> studentList = session.selectList("test.student.getStudentListBy", paramMap, rowBounds);
      
      assertEquals(5, studentList.size());
      assertEquals("李四", studentList.get(0).getName());
      assertEquals("王五", studentList.get(1).getName());
      assertEquals("赵六", studentList.get(2).getName());
      assertEquals("孙七", studentList.get(3).getName());
      assertEquals("吴九", studentList.get(4).getName());
    }
    
    // 嵌套查询
    {
      RowBounds rowBounds = new RowBounds(1, 2);
      List<Score> scoreList = session.selectList("test.score.getScoreList", null, rowBounds);
      
      assertEquals(2, scoreList.size());
      
      Score score1 = scoreList.get(0);
//      assertEquals("数学", score1.getSubject());
      assertEquals(89F, (float) score1.getScore(), 0.01F);
      assertNotNull(score1.getStudent());
      assertEquals(3, (int) score1.getStudent().getId());
      assertEquals("张三", score1.getStudent().getName());
      assertEquals(22, (int) score1.getStudent().getAge());
      
      Score score2 = scoreList.get(1);
//      assertEquals("语文", score2.getSubject());
      assertEquals(92F, (float) score2.getScore(), 0.01F);
      assertNotNull(score2.getStudent());
      assertEquals(4, (int) score2.getStudent().getId());
      assertEquals("李四", score2.getStudent().getName());
      assertEquals(25, (int) score2.getStudent().getAge());
    }
    
    // 对 Mapper 接口查询分页
    {
      RowBounds rowBounds = new RowBounds(3, 5);
      List<Student> studentList = session.getMapper(StudentMapper.class).getStudentList(rowBounds);
    
      assertEquals(5, studentList.size());
      assertEquals("李四", studentList.get(0).getName());
      assertEquals("王五", studentList.get(1).getName());
      assertEquals("赵六", studentList.get(2).getName());
      assertEquals("孙七", studentList.get(3).getName());
      assertEquals("周八", studentList.get(4).getName());
    }
  }

}
