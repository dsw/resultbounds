
CREATE DATABASE `resultbounds`;
USE `resultbounds`;

CREATE TABLE `score` (
  `student_id` int(11) NOT NULL COMMENT '学生ID',
  `subject` varchar(50) NOT NULL COMMENT '科目',
  `score` decimal(4,1) NOT NULL COMMENT '分数'
);

INSERT INTO `score` (`student_id`, `subject`, `score`) VALUES
  (3, '语文', 91.0),
  (3, '数学', 89.0),
  (4, '语文', 92.0),
  (4, '数学', 88.0);

CREATE TABLE `student` (
  `id` int(11) NOT NULL COMMENT '学生ID',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `age` int(11) NOT NULL COMMENT '年龄'
);

INSERT INTO `student` (`id`, `name`, `age`) VALUES
  (1, '刘一', 23),
  (2, '陈二', 21),
  (3, '张三', 22),
  (4, '李四', 25),
  (5, '王五', 22),
  (6, '赵六', 21),
  (7, '孙七', 22),
  (8, '周八', 26),
  (9, '吴九', 24),
  (10, '郑十', 20);
