package com.garbagecode.resultbounds.testdependency;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class MyBatisTestCase {
  private static SqlSession session;
  
  protected SqlSession getSession() {
    if (session == null) {
      session = openSession();
    }
    
    return session;
  }
  
  private SqlSession openSession() {
    String resource = "com/garbagecode/resultbounds/testdependency/mybatis-config.xml";
    InputStream inputStream;

    try {
      inputStream = Resources.getResourceAsStream(resource);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    return sessionFactory.openSession();
  }

}
