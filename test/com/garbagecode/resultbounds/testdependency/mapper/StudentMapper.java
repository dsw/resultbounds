package com.garbagecode.resultbounds.testdependency.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.session.RowBounds;

import com.garbagecode.resultbounds.testdependency.Student;

public interface StudentMapper {

  @Select("select * from student")
  List<Student> getStudentList(RowBounds rowBounds);

}
